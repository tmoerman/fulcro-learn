(ns fulcro-learn.queries-and-idents
  (:require [devcards.core :as rc :refer-macros [defcard]]
            [fulcro.client.mutations :refer [defmutation]]
            [fulcro.client.dom :as dom]
            [fulcro.events :refer [enter-key?]]
            [fulcro.client.cards :refer [defcard-fulcro]]
            [fulcro.client.primitives :as prim :refer [defsc]]
            [fulcro-learn.ui.components :as comp]))

;; these mutations do not make sense anymore...

(defmutation change-item-label [{:keys [text]}]
  (action [{:keys [state]}]
    (swap! state assoc :item/label text)))

(defmutation toggle-complete [{:keys [_]}]
  (action [{:keys [state]}]
    (swap! state update :item/complete? not)))

(defmutation finish-editing [_]
  (action [{:keys [state]}]
    (swap! state assoc :ui/editing? false)))

(defmutation start-editing [_]
  (action [{:keys [state]}]
    (swap! state assoc :ui/editing? true)))

; in a component's destructuring of the data, you will get a figwheel error if there's something that is not queried
(defsc TodoItem [this {:keys [db/id item/label item/complete? ui/editing?]}]
  {:query         [:db/id :item/label :item/complete? :ui/editing?]
   :ident         [:todo-item/by-id :db/id]
   :initial-state (fn [{:keys [id label]}]
                    {:db/id          id
                     :item/label     label
                     :item/complete? false :ui/editing? false})}
  (dom/li nil
    (if editing?
      (dom/input #js {:type      "text"
                      :onChange  (fn [evt] (prim/transact! this `[(change-item-label {:text ~(.. evt -target -value)})]))
                      :onKeyDown (fn [evt]
                                   (if (enter-key? evt)
                                     (prim/transact! this `[(finish-editing {})])))
                      :value     label})
      (dom/span nil
        (dom/input #js {:type    "checkbox"
                        :onClick (fn [evt]
                                   (prim/transact! this `[(toggle-complete {})]))
                        :checked complete?})
        (dom/span
          #js {:onClick (fn [evt] (prim/transact! this `[(start-editing {})]))}
          label)))))

(def ui-todo-item (prim/factory TodoItem {:keyfn :db/id}))

(defsc TodoList [this {:keys [db/id list/name list/items] :as props}]
  {:query         [:db/id :list/name {:list/items (prim/get-query TodoItem)}]
   :ident         [:todo-list/by-id :db/id]
   :initial-state (fn [{:keys [id name]}] {:db/id      id
                                           :list/name  name
                                           :list/items [(prim/get-initial-state TodoItem {:id 1 :label "play tuba"})
                                                        (prim/get-initial-state TodoItem {:id 2 :label "get milk"})
                                                        (prim/get-initial-state TodoItem {:id 3 :label "buy t-shirt"})]})}
  (js/console.log props)
  (dom/div nil
    (dom/h4 nil name)
    (mapv ui-todo-item items)))

(def ui-todo-list (prim/factory TodoList))

(defsc Root [this {:keys [ui/react-key root/todo-list]}]
  {:query         [:ui/react-key {:root/todo-list (prim/get-query TodoList)}]
   :initial-state (fn [_] {:root/todo-list (prim/get-initial-state TodoList {:id 1 :name "My list"})})}
  (dom/div #js {:key react-key}
    (ui-todo-list todo-list)))

(defcard-fulcro queries-and-idents
  "hello"
  Root
  {}
  {:inspect-data true})

