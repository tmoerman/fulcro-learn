(ns fulcro-learn.intro
  (:require [devcards.core :as rc :refer-macros [defcard]]
            [fulcro.client.dom :as dom]
            [fulcro.client.cards :refer [defcard-fulcro]]
            [fulcro.client.primitives :as prim :refer [defsc]]
            [fulcro-learn.ui.components :as comp]))

(defcard SVGPlaceholder
  "# SVG Placeholder"
  (comp/ui-placeholder {:w 200 :h 200}))

(defn render-thing [{:keys [name]}]
  (dom/div #js {:className "boo"}
    (dom/b nil (str "hi " name))))

(defsc ThingRenderer [this {:keys [name]}]
  (dom/div #js {:className "boo"}
    (dom/i nil
      (str "hi " name))))

(def render-thing-factory (prim/factory ThingRenderer))

(defcard my-card
  "# stuff
   * documentation about stuff
  "
  (render-thing-factory {:name "Thomas"}))

;; =============================================

(defsc TodoItem [this {:keys [db/id item/label item/complete? ui/editing?]}]
  (dom/li nil
    (if editing?
      (dom/input #js {:type "text"
                      :value label})
      (dom/span nil
        (dom/input #js {:type "checkbox"
                        :checked complete?})
        label))))

(def ui-todo-item (prim/factory TodoItem))

(defcard todo-item-checked
  (ui-todo-item {:db/id 1
                 :item/label "learn fulcro"
                 :item/complete? true
                 :ui/editing? false}))

(defcard todo-item-unchecked
  (ui-todo-item {:db/id 2
                 :item/label "buy milk"
                 :item/complete? false
                 :ui/editing? false}))

(defcard todo-item-edit-in-progress
  (ui-todo-item {:db/id 2
                 :item/label "buy milk"
                 :item/complete? false
                 :ui/editing? true}))

(defsc Root [this {:keys [name] :as props}]
  {:initial-state {:name "Le Kevain"}}
  (dom/div #js {}
    (str "hi '" name "'")))

(defcard-fulcro your-card
  Root                                                      ; UI component in the root
  {}                                                        ; data in the database
  {:inspect-data true}                                      ; component inspection
  )