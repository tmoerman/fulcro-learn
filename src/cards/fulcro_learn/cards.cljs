(ns fulcro-learn.cards
  (:require fulcro-learn.intro
            fulcro-learn.mutations-part1
            fulcro-learn.queries_and_idents
            fulcro-learn.mutations-part2))
