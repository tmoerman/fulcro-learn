(ns fulcro-learn.mutations-part2
  (:require [devcards.core :as rc :refer-macros [defcard]]
            [fulcro.client.mutations :as m :refer [defmutation]]
            [fulcro.client.dom :as dom]
            [fulcro.events :refer [enter-key?]]
            [fulcro.client.cards :refer [defcard-fulcro]]
            [fulcro.client.primitives :as prim :refer [defsc]]
            [fulcro-learn.ui.components :as comp]))

;; these mutations do not make sense anymore...

(defn item-path [id field] [:todo-item/by-id id field])

(defn change-item-label* [state-map id text]
  (assoc-in state-map (item-path id :item/label) text))

(defmutation change-item-label [{:keys [id text]}]
  (action [{:keys [state]}]
    (swap! state change-item-label* id text)))

(defmutation toggle-complete [{:keys [id]}]
  (action [{:keys [state]}]
    (swap! state update-in (item-path id :item/complete?) not)))

(defmutation start-editing [{:keys [id]}]
  (action [{:keys [state]}]
    (swap! state assoc-in (item-path id :ui/editing?) true)))

(defmutation finish-editing [{:keys [id]}]
  (action [{:keys [state]}]
    (swap! state assoc-in (item-path id :ui/editing?) false)))

; in a component's destructuring of the data, you will get a figwheel error if there's something that is not queried
(defsc TodoItem [this {:keys [db/id item/label item/complete? ui/editing?]}]
  {:query         [:db/id :item/label :item/complete? :ui/editing?]
   :ident         [:todo-item/by-id :db/id]
   :initial-state (fn [{:keys [id label]}]
                    {:db/id          id
                     :item/label     label
                     :item/complete? false :ui/editing? false})}
  (dom/li nil
    (if editing?
      (dom/input #js {:type      "text"
                      :onChange  (fn [evt] (prim/transact! this `[(change-item-label {:id ~id :text ~(.. evt -target -value)})]))
                      :onKeyDown (fn [evt]
                                   (if (enter-key? evt)
                                     ;(prim/transact! this `[(finish-editing {:id ~id})])
                                     (m/toggle! this :ui/editing?)

                                     ))
                      :value     label})
      (dom/span nil
        (dom/input #js {:type    "checkbox"
                        :onClick (fn [_] (prim/transact! this `[(toggle-complete {:id ~id})]))
                        :checked complete?})
        (dom/span
          #js {:onClick (fn [_]
                          ;(prim/transact! this `[(start-editing {:id ~id})])
                          (m/toggle! this :ui/editing?)
                          )}
          label)))))

(def ui-todo-item (prim/factory TodoItem))

(defsc TodoList [this {:keys [db/id list/name list/items] :as props}]
  {:query         [:db/id :list/name {:list/items (prim/get-query TodoItem)}]
   :ident         [:todo-list/by-id :db/id]
   :initial-state (fn [{:keys [id name]}] {:db/id      id
                                           :list/name  name
                                           :list/items [(prim/get-initial-state TodoItem {:id 1 :label "play tuba"})
                                                        (prim/get-initial-state TodoItem {:id 2 :label "get milk"})
                                                        (prim/get-initial-state TodoItem {:id 3 :label "buy t-shirt"})]})}
  (js/console.log props)
  (dom/div nil
    (dom/h4 nil name)
    (mapv ui-todo-item items)))

(def ui-todo-list (prim/factory TodoList))

(defsc Root [this {:keys [ui/react-key root/todo-list]}]
  {:query         [:ui/react-key {:root/todo-list (prim/get-query TodoList)}]
   :initial-state (fn [_] {:root/todo-list (prim/get-initial-state TodoList {:id 1 :name "My list"})})}
  (dom/div #js {:key react-key}
    (ui-todo-list todo-list)))

(defcard-fulcro mutations-part-2
  "hello"
  Root
  {}
  {:inspect-data true})

