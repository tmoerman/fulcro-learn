(ns fulcro-learn.client-test-main
  (:require fulcro-learn.tests-to-run
            [fulcro-spec.selectors :as sel]
            [fulcro-spec.suite :as suite]))

(enable-console-print!)

(suite/def-test-suite client-tests {:ns-regex #"fulcro-learn..*-spec"}
  {:default   #{::sel/none :focused}
   :available #{:focused}})

